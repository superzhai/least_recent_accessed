package main

import ("os"
        "fmt"
        "bytes"
	"syscall"
	"time"
	"io/ioutil"
	"strconv"
	"os/exec"
	)

/* return true if the filename was read in recent day days */
func wasAccessed(day int, filename string) bool {


	fi,err := os.Stat(filename)
	if err != nil {
		fmt.Printf("Cannot find file %s\n", filename)
		return false
	}

        m :=fi.ModTime()
	stat := fi.Sys().(*syscall.Stat_t)
	a := time.Unix(int64(stat.Atim.Sec), int64(stat.Atim.Nsec))



	/* retrieve current time*/
	n := time.Now()
	
	/* compare a with n
	   if a is within (n - day days) to n, then return true, otherwise, return false
	*/
	b := n.Add(-time.Duration(day)*time.Hour*time.Duration(24))

	ret := a.After(b)

	os.Chtimes(filename, a, m)

	return ret

}

func main() {



	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s DAYS\n", os.Args[0]);
		fmt.Println("Purge the file/dir that was accessed within the period starting from DAYS days before until now")
		fmt.Printf("For instance, %s 5 will remove the files/dirs that are not accessed in recent 5 days.", os.Args[0])
		os.Exit(-1)
	}

 	day,_:= strconv.Atoi(os.Args[1])

	if day <1 {
		day = 1
	}

	files,_ := ioutil.ReadDir(".")


	for _, file:= range files {

		b := wasAccessed(day, file.Name()) 

		if b != true { 

			fmt.Println(file.Name())
			c := exec.Command("ls",file.Name())
			var out bytes.Buffer
			c.Stdout = &out
			c.Run()

			fmt.Println(out.String())
		}
	}

}
